# BinderEnvironments

This repository collects [binder](https://mybinder.org) environments to run Jupyter notebooks stored in the repository [mathrepo](https://gitlab.mis.mpg.de/rok/mathrepo).

To enable different environments for each notebook please create a separate branch for each notebook.

